<template>
  <div id="checkout">
    <div class="container">
      <div class="row" v-show="!isThankYouPage">
        <div class="col-sm-12 col-md-6 col-xs-12 pb70 ck-info">
          <personal-details
            class="line relative"
            :is-active="activeSection.personalDetails"
            :focused-field="focusedField"
          />
          <shipping
            class="line relative"
            :is-active="activeSection.shipping"
            v-if="!isVirtualCart"
          />
          <payment class="line relative" :is-active="activeSection.payment" />
          <order-review
            class="line relative"
            :is-active="activeSection.orderReview"
          />
          <div id="custom-steps" />
        </div>
        <div class="hidden-xs col-lg-6 col-sm-6 bg-cl-white ck-summary">
          <cart-summary />
        </div>
      </div>
    </div>
    <thank-you-page v-show="isThankYouPage" />
  </div>
</template>

<script>
import Checkout from "@vue-storefront/core/pages/Checkout";

import PersonalDetails from "theme/components/core/blocks/Checkout/PersonalDetails";
import Shipping from "theme/components/core/blocks/Checkout/Shipping";
import Payment from "theme/components/core/blocks/Checkout/Payment";
import OrderReview from "theme/components/core/blocks/Checkout/OrderReview";
import CartSummary from "theme/components/core/blocks/Checkout/CartSummary";
import ThankYouPage from "theme/components/core/blocks/Checkout/ThankYouPage";
import { registerModule } from "@vue-storefront/core/lib/modules";
import { OrderModule } from "@vue-storefront/core/modules/order";

export default {
  components: {
    PersonalDetails,
    Shipping,
    Payment,
    OrderReview,
    CartSummary,
    ThankYouPage
  },
  mixins: [Checkout],
  beforeCreate() {
    registerModule(OrderModule);
  },
  methods: {
    notifyEmptyCart() {
      this.$store.dispatch("notification/spawnNotification", {
        type: "warning",
        message: this.$t(
          "Shopping cart is empty. Please add some products before entering Checkout"
        ),
        action1: { label: this.$t("OK") }
      });
    },
    notifyOutStock(chp) {
      this.$store.dispatch("notification/spawnNotification", {
        type: "error",
        message: chp.name + this.$t(" is out of stock!"),
        action1: { label: this.$t("OK") }
      });
    },
    notifyNotAvailable() {
      this.$store.dispatch("notification/spawnNotification", {
        type: "error",
        message: this.$t("Some of the ordered products are not available!"),
        action1: { label: this.$t("OK") }
      });
    },
    notifyStockCheck() {
      this.$store.dispatch("notification/spawnNotification", {
        type: "warning",
        message: this.$t(
          "Stock check in progress, please wait while available stock quantities are checked"
        ),
        action1: { label: this.$t("OK") }
      });
    },
    notifyNoConnection() {
      this.$store.dispatch("notification/spawnNotification", {
        type: "warning",
        message: this.$t(
          "There is no Internet connection. You can still place your order. We will notify you if any of ordered products is not available because we cannot check it right now."
        ),
        action1: { label: this.$t("OK") }
      });
    }
  }
};
</script>

<style lang="scss">
@import "~theme/css/base/text";
@import "~theme/css/variables/colors";
@import "~theme/css/helpers/functions/color";
$bg-secondary: color(secondary, $colors-background);
$color-tertiary: color(tertiary);
$color-secondary: color(secondary);
$color-error: color(error);
$color-white: color(white);
$color-black: color(black);
$gray-E5E5E5: color(gray-E5E5E5);
$black-222222: color(black-222222);
$green-95BF47: color(green-95BF47);
$blue-02AFCE: color(blue-02AFCE);

#checkout {
  position: relative;

  .number-circle {
    width: 35px;
    height: 35px;
  }
  .ck-summary {
    box-shadow: -4px 0 10px 0 rgba(0,0,0,0.10);
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    overflow-y: auto;
  }
  .radioStyled {
    display: block;
    color: $black-222222;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }

    .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 16px;
      width: 16px;
      border-radius: 50%;
      border: 1px solid $blue-02AFCE;

      &:after {
        content: "";
        position: absolute;
        display: none;
        top: 2px;
        left: 2px;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        background: $blue-02AFCE;
      }
    }

    input:checked ~ .checkmark:after {
      display: block;
    }
  }

  .select-wrapper {
    select {
      padding-left: 0;
      background: none;
    }
  }

  input[type="checkbox"] + label {
    line-height: 1;
  }

  input[type="checkbox"] + label:before {
    width: 13px;
    height: 13px;
    border-radius: 2px;
  }

  input:checked + label:before {
    border-color: $blue-02AFCE;
    width: 13px;
    height: 13px;
    border-radius: 2px;
  }

  input:checked + label:after {
    width: 7px;
    height: 4px;
    top: 6px;
    left: 3px;
    border-width: 2px;
  }

  input:checked + label:after,
  input:checked + label:before {
    background-color: $blue-02AFCE;
  }
}

.line {
  &:after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 37px;
    z-index: -1;
    width: 1px;
    height: 100%;
    background-color: $bg-secondary;

    @media (max-width: 768px) {
      display: none;
    }
  }
}

.checkout-title {
  @media (max-width: 767px) {
    background-color: $bg-secondary;
    margin-bottom: 25px;

    h1 {
      font-size: 36px;
    }
  }
}
.personal-details,
.shipping-section,
.payment {
  .no-input-icon {
    border: 0 !important;
    input {
      font-size: 16px;
      border-bottom: 1px solid $gray-E5E5E5 !important;
      color: $black-222222 !important;
      padding: 0;
    }
  }
}
.ac-btn-container {
  button {
    background-color: $green-95BF47;
    padding: 15px 0 !important;
    text-transform: uppercase;
  }
}
.sp-address {
  p {
    margin: 0;
    font-size: 14px;
    color: $black-222222;
  }
  .sp-p-address {
    margin-top: 15px;
  }
}
.sp-container {
  max-width: 160px;
  border-radius: 4px;
  text-align: center;
}

@media (max-width: 768px) {
  .h3-pl10 {
    padding-left: 10px;
  }
}

</style>
