export default interface BlogsState {
  categories: any[],
  posts: any[],
  featuredPosts: any[]
}
