import { ActionTree } from "vuex";
import BannerState from "../../types/BannerState";
import rootStore from '@vue-storefront/core/store'
import * as types from "./mutation-types";
import { Logger } from "@vue-storefront/core/lib/logger";
import { quickSearchByQuery } from "@vue-storefront/core/lib/search";
import SearchQuery from "@vue-storefront/core/lib/search/searchQuery";
import config from "config";
import { prepareQuery } from "@vue-storefront/core/modules/catalog/queries/common";
import {
  createPageLoadingQuery,
  createSinglePageLoadQuery
} from "@vue-storefront/core/modules/cms/helpers";
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import i18n from '@vue-storefront/i18n'
const alBannerEntityName = config.searchAdapterList.aureateBanner.es_key;
//console.log('alBannerEntityName', alBannerEntityName)
const actions: ActionTree<BannerState, any> = {
  async fetchNewCollection({ commit, dispatch }) {
    //console.log("test action calling")
    //debugger;
    const newProductsQuery = prepareQuery({ queryConfig: "countries" });
    const newProductsResult = await dispatch(
      "product/list",
      {
        query: newProductsQuery,
        size: 8,
        sort: "created_at:desc"
      },
      { root: true }
    );
    const configuredProducts = await dispatch(
      "category-next/configureProducts",
      { products: newProductsResult.items },
      { root: true }
    );
    commit(types.BANNER_FETCH_BANNER, configuredProducts);
  },

  async country(
    { commit },
    {
      filterValues = null,
      filterField = "identifier",
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) 
   {
    console.log("country data from backend")
    let query = createPageLoadingQuery({ filterField, filterValues });
    const pageResponse = await quickSearchByQuery({
      query,
      entityType: "countries",
      excludeFields,
      includeFields
    });
    commit(types.BANNER_FETCH_BANNER, pageResponse.items);
    return pageResponse.items;
  },
  //   console.log("check action calling")
  // debugger;
  // },
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list(
    context,
    {
      filterValues = null,
      filterField = "id",
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    //console.log("test action calling")
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.banners || context.state.banners.length === 0
    ) {
      console.log("banner", "if");
      return quickSearchByQuery({
        query,
        entityType: alBannerEntityName,
        excludeFields,
        includeFields
      })
        .then(resp => {
          console.log("banner", "if", resp.items);
          context.commit(types.BANNER_FETCH_BANNER, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, "banner")();
        });
    } else {
      console.log("banner", "else");
      return new Promise((resolve, reject) => {
        let resp = context.state.banners;
        resolve(resp);
      });
    }
  },
  async getCountrylist (context) {
    console.log("data from magento");
        try {
      let url = rootStore.state.config.aureatelabs.countrylist.endpoint
      console.log("url check :", url)
      debugger;
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
        console.log("url check adjustMultistoreApiUrl:", url)
      }
      Logger.info('Magento 2 REST API Request with Request Data', 'get-country')()
      await fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            Logger.info('Magento 2 REST API Response Data', 'get-country', { data })()
            if (data.code === 200) {
              context.commit(types.BANNER_FETCH_BANNER, data.result)
            } else {
              rootStore.dispatch('notification/spawnNotification', {
                type: 'error',
                message: data.result.message,
                action1: { label: i18n.t('OK') }
              })
            }
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'get-country')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'get-country')()
    }
  }
  // async single ({ getters, commit, dispatch }, { key = 'identifier', 
  // value, excludeFields = null, includeFields = null, skipCache = false, 
  // setCurrent = true }) {
  //   const currentItems = getters.findItems({ key, value })

  //   if (skipCache || !getters.hasItems || !currentItems) {
  //     const pageResponse = await quickSearchByQuery({
  //       query: createSinglePageLoadQuery({ key, value }),
  //       entityType: 'cms_page',
  //       excludeFields,
  //       includeFields
  //     })
  //     if (pageResponse && pageResponse.items && pageResponse.items.length > 0) {
  //       commit(types.CMS_PAGE_ADD_CMS_PAGE, pageResponse.items[0])
  //       if (setCurrent) commit(types.CMS_PAGE_SET_CURRENT, pageResponse.items[0])
  //       return pageResponse.items[0]
  //     }

  //     throw new Error('CMS query returned empty result')
  //   }

  //   if (currentItems) {
  //     if (setCurrent) {
  //       commit(types.CMS_PAGE_SET_CURRENT, currentItems)
  //     }
  //     return currentItems
  //   }
  // },
};
export default actions;
