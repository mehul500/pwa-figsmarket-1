import { Module } from 'vuex'
import CountryState from '../../types/CountryState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const countryModuleStore: Module<CountryState, any> = {
  namespaced: true,
  state: {
    countries: []
  },
  mutations,
  actions,
  getters
}
