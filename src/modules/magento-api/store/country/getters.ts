import CountryState from '../../types/CountryState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CountryState, any> = {
  getCountryList: (state) => state.countries
}
