import { StorefrontModule } from "@vue-storefront/core/lib/modules";
import { countryModuleStore } from "./store/country/index";
// export const KEY = 'aureatelabs-magento-api'
// const BannerModuleStore = {
//   namespaced: true,
//   state: {
//     key: null
//   }
// }
export const countryModule: StorefrontModule = function({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  console.log("Hii Country data!!");
  store.registerModule("country", countryModuleStore);
};
